import 'dart:io';

import 'package:flutter/material.dart';
import 'package:form_validation/src/bloc/provider.dart';
import 'package:form_validation/src/models/producto_model.dart';
import 'package:form_validation/src/utils/utils.dart' as utils;
import 'package:image_picker/image_picker.dart';

class ProductoPage extends StatefulWidget {
  @override
  _ProductoPageState createState() => _ProductoPageState();
}

class _ProductoPageState extends State<ProductoPage> {
  final formKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  File foto;
  ProductosBloc productosBloc;
  ProductoModel producto = new ProductoModel();
  bool _guardando = false;

  @override
  Widget build(BuildContext context) {
    productosBloc = Provider.productosBloc(context);
    final ProductoModel prodData = ModalRoute.of(context).settings.arguments;
    if (prodData != null) {
      producto = prodData;
    }

    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text('Producto'),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.photo_size_select_actual),
              onPressed: () {
                _procesarImagen(ImageSource.gallery);
              }),
          IconButton(
              icon: Icon(Icons.camera_alt),
              onPressed: () {
                _procesarImagen(ImageSource.camera);
              })
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(15.0),
          child: Form(
              key: formKey,
              child: Column(
                children: <Widget>[
                  _mostrarFoto(),
                  _crearNombre(),
                  _crearPrecio(),
                  _crearDisponible(),
                  _crearBoton()
                ],
              )),
        ),
      ),
    );
  }

  Widget _mostrarFoto() {
    if (producto.fotoUrl != null) {
      return FadeInImage(
          placeholder: AssetImage('assets/jar-loading.gif'),
          image: NetworkImage(producto.fotoUrl),
          height: 300.0,
          fit: BoxFit.contain);
    } else {
      return Image(
        image: (foto?.path != null) ? FileImage(File(foto.path)) : new AssetImage('assets/no-image.png'), // Pregunta si la foto tiene valor foto? y después busca si tiene path, si no tiene path muestra la foto asset, si tiene entonces muestra la foto
        height: 300.0,
        fit: BoxFit.cover,
      );
    }
  }

  _procesarImagen(ImageSource source) async {
    foto = await ImagePicker.pickImage(source: source);

    if (foto != null) {
      producto.fotoUrl = null;
    }
    setState(() {});
  }

  Widget _crearNombre() {
    return TextFormField(
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(labelText: 'Producto'),
      onSaved: (value) => producto.titulo = value,
      validator: (value) {
        if (value.length < 3) {
          return 'Ingrese el nombre del producto';
        }
        return null;
      },
      initialValue: producto.titulo,
    );
  }

  Widget _crearPrecio() {
    return TextFormField(
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      decoration: InputDecoration(
        labelText: 'Precio',
      ),
      onSaved: (value) => producto.valor = double.parse(value),
      validator: (value) {
        if (utils.isNumeric(value)) {
          return null;
        }
        return 'Sólo números';
      },
      initialValue: producto.valor.toString(),
    );
  }

  Widget _crearDisponible() {
    return SwitchListTile(
        value: producto.disponible,
        onChanged: (value) {
          setState(() {
            producto.disponible = value;
          });
        },
        title: Text('Disponible'),
        activeColor: Colors.deepPurple);
  }

  Widget _crearBoton() {
    return RaisedButton.icon(
        onPressed: (_guardando) ? null : _submit,
        icon: Icon(Icons.save),
        label: Text('Guardar'),
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
        color: Colors.deepPurple,
        textColor: Colors.white);
  }

  void _submit() async {
    if (!formKey.currentState.validate()) return;
    formKey.currentState
        .save(); // ejecuta el save de todos los textforms fields

    setState(() {
      _guardando = true;
    });

    if (foto != null) {
      producto.fotoUrl = await productosBloc.subirFoto(foto);
    }

    if (producto.id == null) {
      productosBloc.agregarProducto(producto);
    } else {
      productosBloc.actualizarProducto(producto);
    }

    setState(() {
      _guardando = false;
    });
    mostrarSnackbar('Registro guardado');

    Navigator.pop(context);
  }

  void mostrarSnackbar(String message) {
    final snackbar = SnackBar(
      content: Text(message),
      duration: Duration(milliseconds: 1500),
    );

    scaffoldKey.currentState.showSnackBar(snackbar);
  }
}
