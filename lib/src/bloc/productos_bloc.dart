import 'dart:io';

import 'package:form_validation/src/providers/productos_provider.dart';
import 'package:rxdart/subjects.dart';
import 'package:form_validation/src/models/producto_model.dart';

class ProductosBloc {

  final _productosStreamController = new BehaviorSubject<List<ProductoModel>>();
  final _cargandoStreamController = new BehaviorSubject<bool>();

  final _productosProvider = new ProductosProvider();
  
  Stream<List<ProductoModel>> get productosStream => _productosStreamController.stream;
  Stream<bool> get cargando => _cargandoStreamController.stream;

  void cargarProductos() async {
    final productos = await _productosProvider.cargarProductos();
    _productosStreamController.sink.add(productos);
  }

  void agregarProducto(ProductoModel producto) async {
    _cargandoStreamController.sink.add(true);
    await _productosProvider.crearProducto(producto);
    _cargandoStreamController.sink.add(false);
  }

  Future<String> subirFoto(File image) async {
    _cargandoStreamController.sink.add(true);
    final fotoUrl = await _productosProvider.subirImagen(image);
    _cargandoStreamController.sink.add(false);
    return fotoUrl;
  }

  void actualizarProducto(ProductoModel producto) async {
    _cargandoStreamController.sink.add(true);
    await _productosProvider.actualizarProducto(producto);
    _cargandoStreamController.sink.add(false);
  }

  void borrarProducto(ProductoModel producto) async {
    await _productosProvider.borrarProducto(producto);
  }

  dispose() {
    _productosStreamController?.close();
    _cargandoStreamController?.close();
  }

}