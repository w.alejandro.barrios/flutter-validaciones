import 'package:flutter/material.dart';
import 'package:form_validation/src/bloc/login_bloc.dart';
import 'package:form_validation/src/bloc/productos_bloc.dart';
export 'package:form_validation/src/bloc/login_bloc.dart';
export 'package:form_validation/src/bloc/productos_bloc.dart';

class Provider extends InheritedWidget {

  final loginBloc = new LoginBloc();
  final _productosBloc = new ProductosBloc();

  static Provider _instance;
  factory Provider({Key key, Widget child}) {
    if (_instance == null) {
      _instance = new Provider._(key: key, child: child);
    }

    return _instance;
  }

  Provider._({Key key, Widget child}) : super(key: key, child: child);

  //Provider({Key key, Widget child}) : super(key: key, child: child);


  @override
  bool updateShouldNotify(InheritedWidget oldWidget) =>
      true; // Notificar a los hijos cuando hay cambios

  static LoginBloc of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<Provider>().loginBloc; // Busca entre todo el árbol de Widgets un Provider
  }

  static ProductosBloc productosBloc(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<Provider>()._productosBloc; // Busca entre todo el árbol de Widgets un Provider
  }
}
