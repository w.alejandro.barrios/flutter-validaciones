import 'dart:convert';
import 'dart:io';

import 'package:form_validation/src/preferencias_usuario/preferencias_usuario.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:form_validation/src/models/producto_model.dart';
import 'package:mime_type/mime_type.dart';

class ProductosProvider {
  final String _url = 'https://flutter-varios-2a2b6.firebaseio.com';
  final _preferencias = new PreferenciasUsuario();

  Future<bool> crearProducto(ProductoModel producto) async {
    final url = '$_url/productos.json?auth=${_preferencias.token}';

    final resp = await http.post(url, body: productoModelToJson(producto)); // Aquí podemos enviar headers en un Map<String, String> headers = {'auth' : 'token'}

    final decodedData = json.decode(resp.body);
    print(decodedData);
    return true;
  }

  Future<bool> actualizarProducto(ProductoModel producto) async {
    final url = '$_url/productos/${producto.id}.json?auth=${_preferencias.token}';

    final resp = await http.put(url, body: productoModelToJson(producto));

    final decodedData = json.decode(resp.body);
    print(decodedData);
    return true;
  }

  Future<List<ProductoModel>> cargarProductos() async {
    final url = '$_url/productos.json?auth=${_preferencias.token}';

    final resp = await http.get(url);

    final Map<String, dynamic> decodedData = json.decode(resp.body);
    final List<ProductoModel> productos = new List();

    if (decodedData == null) return [];
    if(decodedData['error'] != null) return [];

    decodedData.forEach((id, product) {
      final prodTemp = ProductoModel.fromJson(product);
      prodTemp.id = id;
      productos.add(prodTemp);
    });

    print(productos);
    return productos;
  }

  Future<int> borrarProducto(ProductoModel producto) async {
    final url = '$_url/productos/${producto.id}.json?auth=${_preferencias.token}';
    final resp = await http.delete(url);

    print(resp.statusCode);
    return 1;
  }

  Future<String> subirImagen(File image) async {
    final url = Uri.parse(
        'https://api.cloudinary.com/v1_1/dxqvfn6ft/image/upload?upload_preset=aysmxhkr');
    final mimeType = mime(image.path).split("/");

    final imageUploadRequest = http.MultipartRequest('POST', url);

    final file = await http.MultipartFile.fromPath('file', image.path,
        contentType: MediaType(mimeType[0], mimeType[1]));

    imageUploadRequest.files.add(file);

    final streamResponse = await imageUploadRequest.send();
    final resp =
        await http.Response.fromStream(streamResponse); // Aquí ya viene el JSON
    if (resp.statusCode != 200 && resp.statusCode != 201) {
      print('Algo salio mal');
      print(resp.body);
      return null;
    }

    final responseData = json.decode(resp.body);
    return responseData['secure_url'];
  }
}
